import {
  UPDATE_REF,
  UPDATE_DATE,
  ADD_ITEM,
  UPDATE_ITEM,
  REMOVE_ITEM,
} from '../constants/Constants';

export function updateRef(referenceValue) {
  return { type: UPDATE_REF, data:referenceValue};
}

export function updateDate(dateValue) {
  return { type: UPDATE_DATE, data:dateValue};
}

export function addItem(e) {
  return { type: ADD_ITEM };
}

export function updateProduct(index,value,field) {
  return { type: UPDATE_ITEM, index:index, data:value, property:field};
}

export function removeItem(index) {
  return { type: REMOVE_ITEM, index:index };
}