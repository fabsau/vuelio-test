import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { appStyle } from '../styles/styles';

export default class ProductTableHeader extends Component {

    render() {
        return (
            <View style={appStyle.productTableHeader}>
                <View style={appStyle.itemFieldSmall}>
                    <Text style={appStyle.label}>Id</Text>
                </View>
                <View style={appStyle.itemField}>
                    <Text style={appStyle.label}>Description</Text>
                </View>
                <View style={appStyle.itemFieldSmall}>
                    <Text style={appStyle.label}>Qty</Text>
                </View>
                <View style={appStyle.itemFieldSmall}>
                    <Text style={appStyle.label}>Price</Text>
                </View>
                <View style={appStyle.itemFieldSmall}>
                    <Text style={appStyle.label}>Total</Text>
                </View>
                <View style={appStyle.itemFieldSmall}>
                    <Text style={appStyle.label}></Text>
                </View>
            </View>
        );
    }
}
