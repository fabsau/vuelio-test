import React, { Component, PropTypes } from 'react';
import { View,TextInput,Text } from 'react-native';
import { appStyle } from '../styles/styles';

export default class OrderDate extends Component {
  render() {
    const { onChange,date } = this.props;
    return (
      <View style={appStyle.section}>
        <Text style={appStyle.label}>Date:</Text>
        <TextInput style={appStyle.input} value={date} onChangeText={(text) => {onChange(text)}} />
      </View>
    );
  }
}