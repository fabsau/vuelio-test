import React, { Component, PropTypes } from 'react';
import { View,TextInput,Text } from 'react-native';
import { appStyle } from '../styles/styles';

export default class OrderReference extends Component {

  render() {
    const { reference, onChange } = this.props;

    let error= '';
    if(reference.length == 0) error = 'Required field';

    return (
      <View style={appStyle.section}>
        <Text style={appStyle.label}>Order Reference:</Text>
        <TextInput style={appStyle.input} onChangeText={(text) => {onChange(text)}} />
        <Text style={appStyle.error} >{error}</Text>
      </View>
    );
  }
}
