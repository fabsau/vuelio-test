import React, { Component, PropTypes } from 'react';
import { View, Text, FlatList, TextInput } from 'react-native';
import { Button } from 'react-native-elements';
import { appStyle } from '../styles/styles';

// Components
import ProductTableHeader from './ProductTableHeader';

export default class ProductTable extends Component {

  constructor(props) {

    super(props);
    this.renderItem = this._renderItem.bind(this);
    this.grandTotalCalc = this._grandTotalCalc.bind(this);
    this.grandTotal = 0;
    
  }

  _grandTotalCalc(products) {
    
    this.grandTotal = 0;
    products.map((product, index) => {
      this.grandTotal += product.quantity * product.price;
    });
  
  }

  _keyExtractor = (item, index) => index;

  _renderItem({ item, index }) {

    const { onUpdate, onDelete } = this.props;
    var total = item.quantity * item.price;
    var id = index+1;

    return (
      <View style={appStyle.itemRow} key={index}>
        <View style={appStyle.itemFieldSmall}>
          <TextInput style={appStyle.itemInput} defaultValue={id.toString()} editable={false}/>
        </View>
        <View style={appStyle.itemField}>
          <TextInput style={appStyle.itemInput} defaultValue={item.description} onChangeText={(text) => {onUpdate(index,text,'description')}} />
        </View>
        <View style={appStyle.itemFieldSmall}>
          <TextInput style={appStyle.itemInput} keyboardType={'numeric'} defaultValue={item.quantity.toString()} onChangeText={(text) => {onUpdate(index,text,'quantity')}} />
        </View>
        <View style={appStyle.itemFieldSmall}>
          <TextInput style={appStyle.itemInput} keyboardType={'numeric'} defaultValue={item.price.toString()} onChangeText={(text) => {onUpdate(index,text,'price')}} />
        </View>
        <View style={appStyle.itemFieldSmall}>
          <TextInput style={appStyle.itemInput} defaultValue={total.toString()} editable={false}/>
        </View>
        <View style={appStyle.itemFieldSmall}>
          <Button
            buttonStyle={appStyle.button}
            onPress={(e) => onDelete(index)}
            title="X"
            fontSize={10}
          />
        </View>
      </View>
    )

  }

  render() {
    const { products, onPush } = this.props;

    var empty_message = (<Text></Text>);
    if (products.length == 0) {
      empty_message = (<Text style={appStyle.label}>NO PRODUCT</Text>);
    }

    this.grandTotalCalc(products); 

    return (
      <View style={appStyle.productTable}>
        <View>
            <Text style={appStyle.label}>Products:</Text>
        </View>
        <ProductTableHeader />
        {empty_message}
        <FlatList removeClippedSubviews={false}
            data={products}
            renderItem={this.renderItem}
            keyExtractor={this._keyExtractor}
            ref='_flatList' />
        <View style={appStyle.footer}>
          <View>
            <Button
              onPress={(e) => onPush()}
              title="+"
              buttonStyle={appStyle.addButton}
            />
          </View>
          <View>
            <Text style={appStyle.total}>Grand Total: {this.grandTotal}</Text>
          </View>
        </View>
      </View>
    );
  }
}