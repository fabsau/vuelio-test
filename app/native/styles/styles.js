import { StyleSheet } from 'react-native';

export const appStyle = StyleSheet.create({
  reactNativeWeb: {
    paddingLeft:10
  },
  input:{
    width:200,
    height:30,
    backgroundColor: '#e6e6e6',
    paddingLeft:5
  },
  button:{
    width:30,
    height:30,
    borderRadius:20
  },
  label:{
    fontWeight: 'bold',
    paddingBottom:5
  },
  error:{
    paddingTop:5,
    color:'red'
  },
  section:{
    marginTop:15
  },
  productTable:{
    marginTop:15
  },
  productTableHeader:{
    marginTop:15,
    paddingLeft:5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  actionButton:{
    width:20,
    borderRadius:4,
    backgroundColor:'#3D6DCC',

  },
  itemFieldSmall:{
    width:45,
    padding:1
  },
  itemField:{
    width:130,
    padding:1,
  },
  itemRow:{
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemInput:{
    height:30,
    borderColor: '#999',
    borderWidth: 1,
    borderRadius:3,
    paddingLeft:2,
    textAlign:'center'
  },
  footer:{
    flex: 1,
    flexDirection: 'row',
    marginTop:40,
  },
  addButton:{
      width:35,
      height:35,
      borderRadius:20
  },
  total:{
    fontWeight:'bold',
    fontSize:18,
    lineHeight:35
  }

});
