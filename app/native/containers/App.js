import React, { Component, PropTypes } from 'react';
import { View } from 'react-native';
import { connect }  from 'react-redux';
import { appStyle } from '../styles/styles';

// Components
import {Header} from 'react-native-elements';
import OrderReference from '../components/OrderReference';
import OrderDate from '../components/OrderDate';
import ProductTable from '../components/ProductTable';

// Actions
import {
  updateRef,updateDate,addItem,removeItem,updateProduct
} from '../../actions/actions';

class OrderScreen extends Component {
  render() {
    
    const { dispatch, reference, date, products } = this.props;

    return (
      <View >
        <Header
          centerComponent={{ text: 'Vuelio Test', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: '#3D6DCC' }}
          innerContainerStyles={{ justifyContent: 'space-around',}}
        />
        <View style={appStyle.reactNativeWeb}>
          
          <OrderReference
            reference={reference}
            onChange={(e) => dispatch(updateRef(e))}
          />
          <OrderDate
            onChange={(e) => dispatch(updateDate(e))} 
            date={date}
          />
          <ProductTable
            onPush={(e) => dispatch(addItem(e))}
            onDelete={(index) => dispatch(removeItem(index))}
            onUpdate={(e,index,field) => dispatch(updateProduct(e,index,field))}
            products={products}
          />
        </View>
      </View>
    );
  }
}

const select = state => state;

// Wrap the component to inject dispatch and state into it
export default connect(select)(OrderScreen);
