import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

// Components
import Header     from '../components/Header';
import OrderReference from '../components/OrderReference';
import OrderDate from '../components/OrderDate';
import ProductTable from '../components/ProductTable';

// Actions
import {
  updateRef,updateDate,addItem,removeItem,updateProduct
} from '../../actions/actions';

class OrderScreen extends Component {
  render() {
    const { dispatch, reference, date, products  } = this.props;
    console.log('proo',products);
    return (
      <div className="react-native-web">
        <Header />
        <OrderReference
          reference={reference}
          onChange={(e) => dispatch(updateRef(e))}
        />
        <OrderDate
          onChange={(e) => dispatch(updateDate(e))} 
          date={date}
        />
        <ProductTable
          onPush={(e) => dispatch(addItem(e))}
          onDelete={(index) => dispatch(removeItem(index))}
          onUpdate={(e,index,field) => dispatch(updateProduct(e,index,field))}
          products={products}
        />
      </div>
    );
  }
}

const select = state => state;

// Wrap the component to inject dispatch and state into it
export default connect(select)(OrderScreen);
