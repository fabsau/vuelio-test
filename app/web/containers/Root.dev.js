import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import OrderScreen from './App';

export default class Root extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <div>
          <OrderScreen />
        </div>
      </Provider>
    );
  }
}