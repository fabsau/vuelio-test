import React, { Component } from 'react';

export default class ProductTable extends Component {

  render() {
    const { products, onUpdate, onPush, onDelete } = this.props;
    let grandTotal = 0;

    if (products.length > 0) {
      var listProducts = products.map((product, index) => {

        var total = product.quantity * product.price;
        grandTotal += total;

        return (
          <div className="item" key={index}>
            <div className="field"><input type="text" value={index + 1} name="id" id="id" disabled /></div>
            <div className="field"><input type="text" value={product.description} name="description" id="description" onChange={(e) => onUpdate(index, e.target.value, 'description')} /></div>
            <div className="field"><input type="number" value={product.quantity} name="quantity" id="quantity" onChange={(e) => onUpdate(index, e.target.value, 'quantity')} /></div>
            <div className="field"><input type="number" value={product.price} name="price" id="price" onChange={(e) => onUpdate(index, e.target.value, 'price')} /></div>
            <div className="field"><input type="number" value={total} name="total" id="total" disabled /></div>
            <div className="field"><button onClick={(e) => onDelete(index)}>X</button></div>
          </div>
        );
      });
      var empty_message = '';
    }
    else {
      var empty_message = 'NO PRODUCT';
      var listProducts = '';
    }

    return (
      <div className="product-table">
        <div className="head" >
          <div className="title">Item No</div>
          <div className="title">Description</div>
          <div className="title">Qty</div>
          <div className="title">Price Per Each</div>
          <div className="title">Total</div>
        </div>
        {listProducts}
        <div className="empty">{empty_message}</div>
        <div className="footer">
          <div className=""><button onClick={(e) => onPush(e)}>+</button></div>
          <div className="total-line">Grand Total: <input type="number" value={grandTotal} name="total" id="total" disabled /></div>
        </div>
      </div>
    );
  }
}