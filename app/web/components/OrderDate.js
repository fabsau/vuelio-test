import React, { Component } from 'react';

export default class OrderDate extends Component {
  render() {
    const { onChange,date } = this.props;
    return (
      <div>
        <label>Date:</label>
        <input type="text" name="date" id="date" value={date} onChange={(e) => {onChange(e.target.value)}} />
      </div>
    );
  }
}