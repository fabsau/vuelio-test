import React, { Component } from 'react';

export default class OrderReference extends Component {

  render() {
    const { reference, onChange } = this.props;

    let error= '';
    if(reference.length == 0) error = 'Required field';

    return (
      <div className="order-reference">
        <label>Order Reference:</label>
        <input type="text" name="reference" id="reference" onChange={(e) => {onChange(e.target.value)}} />
        <span className="error" >{error}</span>
      </div>
    );
  }
}