import assign       from 'object-assign';
import {
  UPDATE_REF,
  UPDATE_DATE,
  ADD_ITEM,
  REMOVE_ITEM,
  UPDATE_ITEM
} from '../constants/Constants';

/**
 * NOTE: It's cool also to use Moment for that :-)
 */
var today = new Date();

const initialState = {
  reference: '',
  date: today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear(),
  products: []
};

export default function reduce(state = initialState, action) {
  switch (action.type) {
    
  case UPDATE_REF:
    return assign({}, state, {
      reference: action.data
    });
  
  case UPDATE_DATE:
    return assign({}, state, {
      date: action.data
    });

  case UPDATE_ITEM:
    var newProducts = state.products.map((product, sidx) => {
      if (action.index !== sidx) return product;
      product[action.property] = action.data;
      return product;
    });
    return assign({}, state, {
      products: newProducts
    });

  case REMOVE_ITEM:
    return assign({}, state, {
      products: state.products.filter((s, _idx) => _idx !== action.index)
    });

  case ADD_ITEM:
    let newId = state.products.length+1;
    return assign({}, state, {
      products: state.products.concat([{id:newId,description:'',quantity:'0',price:'0'}])
    });

  default:
    return state;
  }
}
