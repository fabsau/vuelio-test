export const UPDATE_REF = 'UPDATE_REF';
export const UPDATE_DATE = 'UPDATE_DATE';
export const ADD_ITEM = 'ADD_ITEM';
export const UPDATE_ITEM = 'UPDATE_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';